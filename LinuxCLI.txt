wsl to access on Linux

Ctrl + C : destroy command
Ctrl + R : find command
--help : show all command

cd tab tab : recommended
cd .. : back to parent folder
cd - : back
cd / : quay lại thư mục gốc

ls : list file
ls -l : list all file with permission
ls -la :(full include hiden file)
ls -R : supper details

mkdir folername: create new folder
mkdir -p folderParent/folderChild1/folderChild2 : Create nested folder
rmdir foldername: delete folder
rm -r foldername: delete all of element of folder

touch filename: create single file
rm filename: delete file

cat filename: check data of file (cat able to open multi file)
cat filename1 filename2 > newfilename : concat 2 file to new file
cat filename | grep "keyword" : search content

echo "content" : write content
echo "<h1>Hello world</h1>" > test.html : create a new file with content (content overriden)
			    >>		: content append

tail filename: check data of 10 lasted element
tail -n number filename: ...number.....
tail --help: all of command of tail

cp filename filenamecopy : copy file
cp -r foldername: copy folder 

mv filename newfilename: move (or rename file)
mv filename folder/[newNameOptinal] : move file to folder

-----------admin CML------------
-> ls -l : for controll permission of file
-> NOTE: Sudo only  accessible in Ubuntu terminal, with Window Power Shell we dont need to sudo :"))
- username: mladev | password: 123

sudo [command]: Super user permission

sudo chmod u=rwx,g=rx,o=r filename: add permission (r: read, w: write, x: execute | u: user, g: group, o: orther | = assign, + add more, - remove)
sudo chmod 777 filename : add permission using octal (4: read, 2: write, 1: execute, 0: not per | rwx -> 7, rx -> 5)

sudo chown OwnerName filename : move permission for other owner
sudo chown OwnerName:OwnerName filename : move permission for other owner and group-er

groups : check user group
cat /etc/group : show all groups
-----------------------------------

man [command] : detail of command (only working in ubuntu terminal), same to --help

wget link: download link to current index
sudo apt install packagename : download package (-y : optional for access full permission)

ps aux : same to task manager (to get PID and more in4 of task process)
kill [PID] : end task (optinal: -9: exit ! | -15: defaut, exit after save in4)
top: same to ps axu but more detail
df : check hardware (-h: ez to check)
free: check ram (-h: ez to check)

ping : check status of task
uname -a: check name
passswd: change password


----------code combination and run -----------
g++ filename - o
./o

